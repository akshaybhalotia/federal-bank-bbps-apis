# frozen_string_literal: true

require 'uri'
require 'openssl'
require 'net/http'
require 'json'

body_hash = {
  # identification of request type
  'RequestType' => 'some_identifier',
  'MerchantValidationRequestDetails' => {
    # IP address of the request intiator
    'RequesterIP' => '131.51.76.41',
    # random unique identifier on merchant end
    'MerchantRefNo' => 'rand_uniq_1',
    # merchant identification code provided by bank / BBPOU
    'MerchantCode' => 'bank_dep_1',
    # unique agent ID of the channel agent provided by bank / BBPOU
    'AgentId' => 'bank_dep_2',
    # user identification provided by bank / BBPOU
    'UserName' => 'bank_dep_3',
    # user password provided by bank / BBPOU
    'UserPass' => 'bank_dep_4',
    # store ID that is initiating this request - ATM/POS will provide Terminal ID, for other channels it will be same
    # as MerchantCode
    'StoreCode' => 'bank_dep_1',
    # biller's ID to validate against
    'BillerId' => '6461220401971200',
    # customer bill details as name value pairs
    'SubscriptionDetails' => [
      {
        'name' => 'Christina Hawkins',
        'value' => '95.77'
      }
    ],
    # undocumented
    'LoginName' => 'Owen Long',
    # undocumented
    'Hash' => 'giartegegadga'
  }
}

url = URI('https://sandgateway.federalbank.co.in/bbps/bill_validation')

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_PEER

request = Net::HTTP::Post.new(url)
request['content-type'] = 'application/json'
request['accept'] = 'application/json'
request.body = body_hash.to_json

response = http.request(request)
puts response.read_body
